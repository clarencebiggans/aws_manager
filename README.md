# aws_instance_manager
Manage AWS Instances using Python


Installation
------------
* Create a python environment with virtualenv
`python3 -m venv env`
* Activate that environment
`source env/bin/activate`
* Install all requirements in `requirements.txt`
`pip install -r requirements.txt`

Ansible Commands
----------------
# Found here:
* https://aws.amazon.com/blogs/apn/getting-started-with-ansible-and-dynamic-amazon-ec2-inventory-management/

* Listing all of the instances on AWS
`/etc/ansible/ec2.py --list`

Ansible Playbooks
-----------------
* https://www.techrepublic.com/article/how-to-write-an-ansible-playbook/
