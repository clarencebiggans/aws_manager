import sys
import time
# This is needed, reason:
# https://stackoverflow.com/questions/30669474/beyond-top-level-package-error-in-relative-import
sys.path.append('.')
from manager import EC2Manager

m = EC2Manager()
instance = m.create()
print('InstanceId: %s' % instance.id)
print('Instance Created, now waiting')

while(True):
  if m.contains(instance.id):
    print('Instance Found')
    break
  print('Instance not found on remote server yet, waiting')
  time.sleep(3)

while(True):
  if len(m.index()['pending']) == 0:
    print('Instances out of pending')
    break;
  print('Instance not out of pending yet')
  time.sleep(5)
