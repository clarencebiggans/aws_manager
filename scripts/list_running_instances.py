import sys
# This is needed, reason:
# https://stackoverflow.com/questions/30669474/beyond-top-level-package-error-in-relative-import
sys.path.append('.')
from manager import EC2Manager

def print_instances(instances, _type='running'):
  print('------------------------%s------------------------------' % _type)
  i = 0
  for instance in instances[_type]:
    if i > 0:
      print('-------------------------')
    print('ID: %s' % instance.id)
    print('Public IP Address: %s' % instance.public_ip_address)
    print('Public DNS: %s' % instance.public_dns_name)
    example_ssh_call = 'ssh -i /etc/aws/%s.pem ubuntu@%s' % (
      instance.key_name, instance.public_dns_name)
    print('Example SSH Call: %s' % example_ssh_call)
    print('Example Ansible Call: ansible ec2 -m ping -u ubuntu')
    i += 1
  print('------------------------------------------------------')

m = EC2Manager()
instances = m.index()
found = False
if len(instances['running']) > 0:
  found = True
  print_instances(instances, _type='running')

if len(instances['pending']) > 0:
  found = True
  print_instances(instances, _type='pending')

if found is False:
  print('No Running Instances Found')
